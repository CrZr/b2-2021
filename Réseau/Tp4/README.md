

```
Pc1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=5.873 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=5.751 ms

Pc1> ping 10.1.1.3

host (10.1.1.3) not reachable
```

# TP4 : Vers un réseau d'entreprise

# I. Dumb switch

## 2. Adressage topologie 1

| Node  | IP            |
|-------|---------------|
| `pc1` | `10.1.1.1/24` |
| `pc2` | `10.1.1.2/24` |

## 3. Setup topologie 1

🌞 **Commençons simple**

- définissez les IPs statiques sur les deux VPCS
```
Pc1> ip 10.1.1.1 255.255.255.0
Checking for duplicate address...
Pc1 : 10.1.1.1 255.255.255.0
```
```
Pc2> ip 10.1.1.2 255.255.255.0
Checking for duplicate address...
Pc2 : 10.1.1.2 255.255.255.0
```

- `ping` un VPCS depuis l'autre
```
Pc1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=7.823 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=5.259 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=5.687 ms
```

# II. VLAN

## 2. Adressage topologie 2

| Node  | IP            | VLAN |
|-------|---------------|------|
| `pc1` | `10.1.1.1/24` | 10   |
| `pc2` | `10.1.1.2/24` | 10   |
| `pc3` | `10.1.1.3/24` | 20   |

### 3. Setup topologie 2

🌞 **Adressage**

- définissez les IPs statiques sur tous les VPCS
```
Pc1 : 10.1.1.1 255.255.255.0
```
```
Pc2 : 10.1.1.2 255.255.255.0
```
```
Pc3 : 10.1.1.3 255.255.255.0
```

- vérifiez avec des `ping` que tout le monde se ping
```
Pc1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=8.538 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=8.375 ms

Pc1> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=9.498 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=6.628 ms
```

🌞 **Configuration des VLANs**

- déclaration des VLANs sur le switch `sw1`
```
Switch(config)#vlan 10
Switch(config-vlan)#
Switch(config-vlan)#name vlan10
```
```
Switch(config)#vlan 20
Switch(config-vlan)#
Switch(config-vlan)#name vlan20
```
- ajout des ports du switches dans le bon VLAN (voir [le tableau d'adressage de la topo 2 juste au dessus](#2-adressage-topologie-2))
```
Switch(config)#interface Gi0/0
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 10
```
```
Switch(config)#interface Gi0/1
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 10
```
```
Switch(config)#interface Gi0/2
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 20
```

🌞 **Vérif**

- `pc1` et `pc2` doivent toujours pouvoir se ping
```
Pc1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=7.916 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=4.178 ms
```

- `pc3` ne ping plus personne
```
Pc1> ping 10.1.1.3

host (10.1.1.3) not reachable
```
```
Pc2> ping 10.1.1.3

host (10.1.1.3) not reachable
```

# III. Routing

## 2. Adressage topologie 3

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `admins`  | `10.2.2.0/24` | 12           |
| `servers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 3

🌞 **Adressage**

- définissez les IPs statiques sur toutes les machines **sauf le *routeur***


🌞 **Configuration des VLANs**
```
Switch(config)#vlan 11
Switch(config-vlan)#name clients
Switch(config-vlan)#exit
Switch(config)#vlan 12
Switch(config-vlan)#name servers
Switch(config-vlan)#exit
Switch(config)#vlan 13
Switch(config-vlan)#name routers
Switch(config-vlan)#exit
Switch(config)#exit
```
```
Switch(config)#interface Gi0/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#exit
Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
```

---

➜ **Pour le *routeur***

🌞 **Config du *routeur***

- attribuez ses IPs au *routeur*
  - 3 sous-interfaces, chacune avec son IP et un VLAN associé
  ```
  R1(config)#interface FastEthernet0/0.11
  R1(config-subif)#encapsulation dot1Q 11
  R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
  R1(config-subif)#exit
  R1(config)#interface FastEthernet0/0.12
  R1(config-subif)#encapsulation dot1Q 12
  R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
  R1(config-subif)#exit
  R1(config)#interface FastEthernet0/0.13
  R1(config-subif)#encapsulation dot1Q 13
  R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
  R1(config-subif)#exit
  ```

🌞 **Vérif**

- tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau
- en ajoutant une route vers les réseaux, ils peuvent se ping entre eux
  - ajoutez une route par défaut sur les VPCS
  - ajoutez une route par défaut sur la machine virtuelle
  - testez des `ping` entre les réseaux
  ```
  adm1> ping 10.1.1.1
  84 bytes from 10.1.1.1 icmp_seq=1 ttl=63 time=25.039 ms
  84 bytes from 10.1.1.1 icmp_seq=2 ttl=63 time=21.385 ms
  84 bytes from 10.1.1.1 icmp_seq=3 ttl=63 time=20.563 ms
  ```

# IV. NAT

## 2. Adressage topologie 4

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `admins`  | `10.2.2.0/24` | 12           |
| `servers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 4

🌞 **Configurez le NAT**

```
R1#conf t
R1(config)#int FastEthernet 1/0
R1(config-if)#ip address dhcp
R1(config-if)#no shut
R1(config-if)#exit
R1(config)#exit
```

🌞 **Test**

- ajoutez une route par défaut (si c'est pas déjà fait)
  - pour chaque VPCS
  ```
  PC1> ip dns 1.1.1.1
  ``` 

  - sur la machine Linux
  on rajoute `nameserver 1.1.1.1` dans `/etc/resolv.conf` pour la vm.

- vérifiez un `ping` vers un nom de domaine
```
PC1> ping google.com
google.com resolved to 172.217.18.206
84 bytes from 172.217.18.206 icmp_seq=1 ttl=127 time=41.002 ms
84 bytes from 172.217.18.206 icmp_seq=2 ttl=127 time=40.514 ms
```