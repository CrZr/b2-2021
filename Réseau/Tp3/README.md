# TP3 : Progressons vers le réseau d'infrastructure

# I. (mini)Architecture réseau

### Tableau des réseaux
| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
|---------------|-------------------|-------------------|-----------------------------|--------------------|-------------------|
| `server1`     | `10.3.0.0`        | `255.255.255.128` | 126                         | `10.3.0.126`       | `10.3.0.127`      |
| `client1`     | `10.3.0.128`      | `255.255.255.192` | 62                          | `10.3.0.190`       | `10.3.0.191`      |
| `server2`     | `10.3.0.192`      | `255.255.255.240` | 14                          | `10.3.0.206`       | `10.3.0.207`      |


### Tableau d'adressage
| Nom machine          | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|----------------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3`         | `10.3.0.190/26`      | `10.3.0.126/25`      | `10.3.0.206/28`      | Carte NAT             |
| `dhcp.client1.tp3`   | `10.3.0.130/26`      | x                    | x                    | `10.3.0.190/26`       |
| `marcel.client1.tp3` | `10.3.0.131/26`      | x                    | x                    | `10.3.0.190/26`       |
| `dns1.server1.tp3`   | x                    | `10.3.0.2/25`        | x                    | `10.3.0.126/25`       |
| `johnny.client1.tp3` | `10.3.0.132/26`      | x                    | x                    | `10.3.0.190/26`       |
| `web1.server1.tp3`   | x                    | x                    | `10.3.0.194/28`      | `10.3.0.206/28`       |
| `nfs1.server1.tp3`   | x                    | x                    | `10.3.0.195/28`      | `10.3.0.206/28`       |

# II. Services d'infra

📁 [dhcpd.conf](https://gitlab.com/CrZr/b2-2021/-/tree/main/R%C3%A9seau/Tp3/Fichiers/dhcpd.conf)

---

🌞 **Depuis `marcel.client1.tp3`**

- prouver qu'il a un accès internet
```
[kai@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=19.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=18.8 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 18.776/18.972/19.168/0.196 ms
```

- résolution de noms, avec des infos récupérées par votre DHCP
```
[kai@marcel ~]$ dig ynov.com
...

;; ANSWER SECTION:
ynov.com.               5601    IN      A       92.243.16.143

;; Query time: 20 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 30 12:50:35 CEST 2021
;; MSG SIZE  rcvd: 53
```

- à l'aide de la commande `traceroute`, prouver que `marcel.client1.tp3` passe par `router.tp3` pour sortir de son réseau
```
[kai@marcel ~]$ sudo traceroute -I 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  _gateway (10.3.0.190)  0.487 ms  0.469 ms  0.466 ms
 2  10.0.2.2 (10.0.2.2)  0.523 ms  0.522 ms  0.611 ms
...
```

## 2. Serveur DNS

🌞 **Mettre en place une machine qui fera office de serveur DNS**

> Vous avez donc 3 fichiers à gérer : le fichier de conf principal `named.conf`. Un fichier de zone forward pour la zone `server1.tp3` : `server1.tp3.forward`. Et un  fichier similaire pour la zone `server2.tp3`.

- Fichier de conf du serveur DNS: `named.conf`
```
[kai@dns1 ~]$ sudo cat /etc/named.conf
options {
        listen-on port 53 { any; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { any; };

        recursion no;

        dnssec-enable yes;
        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "server1.tp3" IN {
        type master;
        file "server1.tp3.forward";
        allow-update { none; };
};

zone "server2.tp3" IN {
        type master;
        file "server2.tp3.forward";
        allow-update { none; };
};



include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

- Fichiers de conf des zones Forward
```
[kai@dns1 ~]$ sudo cat /var/named/server1.tp3.forward
$TTL 86400
@   IN  SOA     dns1.server1.tp3. root.server1.tp3. (
         2021080804  ;Serial
         3600        ;Refresh
         1800        ;Retry
         604800      ;Expire
         86400       ;Minimum TTL
)

@         IN  NS      dns1.server1.tp3.
@         IN  A       10.3.0.2

dns1           IN  A       10.3.0.2
[kai@dns1 ~]$ sudo cat /var/named/server2.tp3.forward
$TTL 86400
@   IN  SOA     dns1.server1.tp3. root.server1.tp3. (
         2021080804  ;Serial
         3600        ;Refresh
         1800        ;Retry
         604800      ;Expire
         86400       ;Minimum TTL
)

@         IN  NS      dns1.server1.tp3.
@         IN  A       10.3.0.2

dns1           IN  A       10.3.0.2
```

🌞 **Tester le DNS depuis `marcel.client1.tp3`**

- définissez **manuellement** l'utilisation de votre serveur DNS
```
[kai@marcel ~]$ cat /etc/resolv.conf
nameserver 10.3.0.2
```

- essayez une résolution de nom avec `dig`
  ```
  [kai@marcel ~]$ dig dns1.server1.tp3

  ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3
  ;; global options: +cmd
  ;; Got answer:
  ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 48482
  ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

  ;; OPT PSEUDOSECTION:
  ; EDNS: version: 0, flags:; udp: 1232
  ; COOKIE: 165f718e8f61888de1bc357d616c8bcb402d255bcbf1cc26 (good)
  ;; QUESTION SECTION:
  ;dns1.server1.tp3.              IN      A

  ;; ANSWER SECTION:
  dns1.server1.tp3.       86400   IN      A       10.3.0.2

  ;; AUTHORITY SECTION:
  server1.tp3.            86400   IN      NS      dns1.server1.tp3.

  ;; Query time: 1 msec
  ;; SERVER: 10.3.0.2#53(10.3.0.2)
  ;; WHEN: Sun Oct 17 22:47:08 CEST 2021
  ;; MSG SIZE  rcvd: 103
  ```

- prouvez que c'est bien votre serveur DNS qui répond pour chaque `dig`
```
    ;; ANSWER SECTION:
    dns1.server1.tp3.       86400   IN      A       10.3.0.2

    ;; AUTHORITY SECTION:
    server1.tp3.            86400   IN      NS      dns1.server1.tp3.

    ;; Query time: 1 msec
>>  ;; SERVER: 10.3.0.2#53(10.3.0.2)
```
```
[kai@dns1 ~]$ ip a
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b7:0b:fd brd ff:ff:ff:ff:ff:ff
>>  inet 10.3.0.2/25 brd 10.3.0.127 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feb7:bfd/64 scope link
       valid_lft forever preferred_lft forever
```

Le serveur dns qui répond est bien dns1.server1.tp3(10.3.0.2)

🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds

- les serveurs, on le fait à la main
```
[kai@router ~]$ sudo cat /etc/resolv.conf
nameserver 10.3.0.2
```
```
[kai@dhcp ~]$ sudo cat /etc/resolv.conf
nameserver 10.3.0.2
```

- les clients, c'est fait *via* DHCP
```
[kai@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
    option domain-name     "client1.tp3";
>>  option domain-name-servers     10.3.0.2;
    default-lease-time 600;
    max-lease-time 7200;
    authoritative;
    subnet 10.3.0.128 netmask 255.255.255.192 {
        range dynamic-bootp 10.3.0.131 10.3.0.189;
        option broadcast-address 10.3.0.191;
        option routers 10.3.0.190;
    }
```

## 3. Get deeper

On va affiner un peu la configuration des outils mis en place.

### A. DNS forwarder

🌞 **Affiner la configuration du DNS**

- vérifier depuis `marcel.client1.tp3` que vous pouvez résoudre des noms publics comme `google.com` en utilisant votre propre serveur DNS (commande `dig`)
```
  [kai@marcel ~]$ dig ynov.com

  ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
  ;; global options: +cmd
  ;; Got answer:
  ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 49200
  ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 3, ADDITIONAL: 1
  
  ;; OPT PSEUDOSECTION:
  ; EDNS: version: 0, flags:; udp: 1232
  ; COOKIE: 2f23243d09a871d1a1224753616c8af1be525faae5334b3e (good)
  ;; QUESTION SECTION:
  ;ynov.com.                      IN      A

  ;; ANSWER SECTION:
  ynov.com.               10800   IN      A       92.243.16.143

  ;; AUTHORITY SECTION:
  ynov.com.               172800  IN      NS      ns-78-c.gandi.net.
  ynov.com.               172800  IN      NS      ns-111-b.gandi.net.
  ynov.com.               172800  IN      NS      ns-177-a.gandi.net.

  ;; Query time: 1070 msec
  ;; SERVER: 10.3.0.2#53(10.3.0.2)
  ;; WHEN: Sun Oct 17 23:12:30 CEST 2021
  ;; MSG SIZE  rcvd: 158
  ```

### B. On revient sur la conf du DHCP

🖥️ **VM johnny.client1.tp3**

🌞 **Affiner la configuration du DHCP**

- créer un nouveau client `johnny.client1.tp3` qui récupère son IP, et toutes les nouvelles infos, en DHCP
```
[kai@johnny ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
BOOTPROTO=dhcp
NAME=enp0s3
DEVICE=enp0s3
ONBOOT=yes
```
```
[kai@johnny ~]$ cat /etc/resolv.conf
; generated by /usr/sbin/dhclient-script
search client1.tp3
nameserver 10.3.0.2
```

---

# III. Services métier

## 1. Serveur Web

🖥️ **VM web1.server2.tp3**

🌞 **Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients**

Le server web que j'ai installé est `nginx`
```
[kai@web1 ~]$ sudo dnf install nginx
[kai@web1 ~]$ sudo systemctl enable --now nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[kai@web1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[kai@web1 ~]$ sudo firewall-cmd --reload
success
```

🌞 **Test test test et re-test**

- testez que votre serveur web est accessible depuis `marcel.client1.tp3`
  - utilisez la commande `curl` pour effectuer des requêtes HTTP
  ```
  [kai@marcel ~]$ curl web1.server2.tp3
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

  [...]
  ```

---

> 👋👋👋 **HEY ! C'est beau là.** On a un client qui consomme un serveur web, avec toutes les infos récupérées en DHCP, DNS, blablabla + un routeur maison.  
Je sais que ça se voit pas trop avec les `curl`. Vous pourriez installer un Ubuntu graphique sur le client si vous voulez vous y croire à fond, avec un ptit Firefox, Google Chrome ou whatever.  
**Mais là on y est**, vous avez un ptit réseau, un vrai, avec tout ce qu'il faut.

## 2. Partage de fichiers

### A. L'introduction wola

Dans cette partie, on va monter un serveur NFS. C'est un serveur qui servira à partager des fichiers à travers le réseau.  

**En d'autres termes, certaines machines pourront accéder à un dossier, à travers le réseau.**

Dans notre cas, on va faire en sorte que notre serveur web `web1.server2.tp3` accède à un partage de fichier.

> Dans un cas réel, ce partage de fichiers peut héberger le site web servi par notre serveur Web par exemple.  
Ou, de manière plus récurrente, notre serveur Web peut effectuer des sauvegardes dans ce dossier. Ainsi, le serveur de partage de fichiers devient le serveur qui centralise les sauvegardes.

### B. Le setup wola

🖥️ **VM nfs1.server2.tp3**

🌞 **Setup d'une nouvelle machine, qui sera un serveur NFS**

Installation de `nfs`
```
[kai@nfs1 ~]$ sudo dnf install nfs-utils -y
[...]
```
```
[kai@nfs1 ~]$ cat /etc/idmapd.conf
[...]
Domain = server2.tp3
[...]
```
```
[kai@nfs1 srv]$ sudo mkdir nfs_share
```
```
[kai@nfs1 srv]$ cat /etc/exports
/srv/nfs_share 10.3.0.192/28(rw,no_root_squash)
```
```
[kai@nfs1 srv]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[kai@nfs1 srv]$ sudo chown kai nfs_share
```
```
[kai@nfs1 srv]$ sudo firewall-cmd --add-service=nfs --permanent
success
[kai@nfs1 srv]$ sudo firewall-cmd --reload
success
```

🌞 **Configuration du client NFS**

- effectuez de la configuration sur `web1.server2.tp3` pour qu'il accède au partage NFS
```
[kai@web1 ~]$ sudo dnf install nfs-utils -y
[...]
```
```
[kai@web1 ~]$ cat /etc/idmapd.conf
[...]
Domain = server2.tp3
[...]
```

- le partage NFS devra être monté dans `/srv/nfs/`
```
[kai@web1 ~]$ sudo mkdir /srv/nfs
[kai@web1 ~]$ sudo mount -t nfs nfs1.server2.tp3:/srv/nfs_share /srv/nfs
```
```
[kai@web1 ~]$ df -hT
Filesystem                      Type      Size  Used Avail Use% Mounted on
[...]
nfs1.server2.tp3:/srv/nfs_share nfs4      6.2G  2.0G  4.2G  33% /srv/nfs
```
```
[kai@web1 ~]$ cat /etc/fstab
[...]
nfs1.server2.tp3:/srv/nfs_share /srv/nfs        nfs     defaults        0 0
```

🌞 **TEEEEST**

- tester que vous pouvez lire et écrire dans le dossier `/srv/nfs` depuis `web1.server2.tp3`
```
[kai@web1 ~]$ cd /srv/nfs/
[kai@web1 nfs]$ nano test
[kai@web1 nfs]$ cat test
HEY ! C'est beau là.
```

- vous devriez voir les modifications du côté de  `nfs1.server2.tp3` dans le dossier `/srv/nfs_share/`
```
[kai@nfs1 ~]$ cd /srv/nfs_share/
[kai@nfs1 nfs_share]$ ls
test
```

> # IV. Un peu de théorie : TCP et UDP
> 
> Bon bah avec tous ces services, on a de la matière pour bosser sur TCP et UDP. P'tite partie technique pure avant de conclure.
> 
> 🌞 **Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP :**
> 
> - SSH
> - HTTP
> - DNS
> - NFS
> 
> 📁 **Captures réseau `tp3_ssh.pcap`, `tp3_http.pcap`, `tp3_dns.pcap` et `tp3_nfs.pcap`**
> 
> > **Prenez le temps** de réfléchir à pourquoi on a utilisé TCP ou UDP pour transmettre tel ou tel protocole. Réfléchissez à quoi > servent chacun de ces protocoles, et de ce qu'on a besoin qu'ils réalisent.
> 
> 🌞 **Expliquez-moi pourquoi je ne pose pas la question pour DHCP.**
> 
> 🌞 **Capturez et mettez en évidence un *3-way handshake***
> 
> 📁 **Capture réseau `tp3_3way.pcap`**

# V. El final

🌞 **Bah j'veux un schéma.**

![](https://i.imgur.com/X0LoAPL.png)

### Tableau des réseaux
| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
|---------------|-------------------|-------------------|-----------------------------|--------------------|-------------------|
| `server1`     | `10.3.0.0`        | `255.255.255.128` | 126                         | `10.3.0.126`       | `10.3.0.127`      |
| `client1`     | `10.3.0.128`      | `255.255.255.192` | 62                          | `10.3.0.190`       | `10.3.0.191`      |
| `server2`     | `10.3.0.192`      | `255.255.255.240` | 14                          | `10.3.0.206`       | `10.3.0.207`      |


### Tableau d'adressage
| Nom machine          | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|----------------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3`         | `10.3.0.190/26`      | `10.3.0.126/25`      | `10.3.0.206/28`      | Carte NAT             |
| `dhcp.client1.tp3`   | `10.3.0.130/26`      | x                    | x                    | `10.3.0.190/26`       |
| `marcel.client1.tp3` | `10.3.0.131/26`      | x                    | x                    | `10.3.0.190/26`       |
| `dns1.server1.tp3`   | x                    | `10.3.0.2/25`        | x                    | `10.3.0.126/25`       |
| `johnny.client1.tp3` | `10.3.0.132/26`      | x                    | x                    | `10.3.0.190/26`       |
| `web1.server1.tp3`   | x                    | x                    | `10.3.0.194/28`      | `10.3.0.206/28`       |
| `nfs1.server1.tp3`   | x                    | x                    | `10.3.0.195/28`      | `10.3.0.206/28`       |

🌞 **Et j'veux des fichiers aussi, tous les fichiers de conf du DNS**

- 📁 [server1.tp3.forward](https://gitlab.com/CrZr/b2-2021/-/tree/main/R%C3%A9seau/Tp3/Fichiers/server1.tp3.forward)
- 📁 [server2.tp3.forward](https://gitlab.com/CrZr/b2-2021/-/tree/main/R%C3%A9seau/Tp3/Fichiers/server2.tp3.forward)
- 📁 [named.conf](https://gitlab.com/CrZr/b2-2021/-/tree/main/R%C3%A9seau/Tp3/Fichiers/named.conf)
