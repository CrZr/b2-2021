#!/bin/bash
# Simple Script Backup Database
# kai 16/11/21

d=$(date +%y%m%d_%H%M%S);
backup_name=tp3_backup_db_$d.tar.gz;

destination=$ ('/srv/backup/');
archive=$ (readlink -f $backup_name);
database=$ ('moodle');

mysqldump -u root > 'moodle';

tar -czvf $backup_name $database;

rsync -av --remove-source-files  $archive $destination;

# Delete more than 5 backup
cd $destination;
ls -tQ  | tail -n+6 | xargs rm &>/dev/null;
cd -;
rm $database;
