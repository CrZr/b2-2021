# TP3: Simple Moodle Server

## Sommaire

- [TP3: Simple Moodle Server](#tp3-simple-moodle-server)
  - [Sommaire](#sommaire)
  - [0. Description](#0-description)
  - [I. Prérequis](#i-pr%C3%A9requis)
  - [II. Déploiement de l'infrastructure](#ii-d%C3%A9ploiement-de-linfrastructure)
  - [III. Backup](#iii-backup)
    - [1. Backup Web](#1-server-web)
    - [2. Backup Db](#2-server-db)
  - [IV. Monitoring](#iv-monitoring-%C3%A0-faire-sur-tout-les-serveurs)

# 0. Description

La solution proposée permet de mettre en place le service [Moodle](https://moodle.org) :

- Un serveur web (Apache)
    -> Déploiement de l'ensemble de l'infrastructure

- Une base de donnée (MySQL)

    -> Requis par l'install de Moodle

- Un service NetData

    -> Monitoring de l'infrastructure

- Un serveur NFS

    -> Backup des fichiers sensibles

## I. Prérequis

**Setup de trois machines Rocky Linux configurées de façon basique.**

- **un accès internet (via la carte NAT)**
  - carte réseau dédiée
  - route par défaut

- **un accès à un réseau local** (via la carte Host-Only)
  - carte réseau dédiée (host-only sur VirtualBox)
  - les machines doivent posséder une IP statique sur l'interface host-only

- **les machines doivent avoir un nom**

- **utiliser un serveur DNS**
  - vérifier avec le bon fonctionnement avec la commande `dig`

- **Ajouter une résolution de nom pour les servers db,web,backup**
    - Exemple
        ```
        10.101.1.11 web web.tp3
        10.101.1.12 db db.tp3
        10.101.1.13 backup backup.tp3
        ```

| Name           | IP            |
|----------------|---------------|
| `web.tp3`      | `10.101.1.11` |
| `db.tp3`       | `10.101.1.12` |
| `backup.tp3`   | `10.101.1.13` |
| Votre hôte     | `10.101.1.1`  |

# II. Déploiement de l'infrastructure

## Server Db

### Installer MySql

```
sudo dnf install @mysql
```

**Création de la base de donnée**

Il est nécessaire de créer une base de données vide (ici « moodle ») dans votre gestionnaire de base de données, ainsi qu'un utilisateur spécial (ici « kai ») ayant accès à cette base de données (et seulement à celle-ci). Il est possible d'utiliser l'utilisateur « root », mais cela n'est pas recommandé sur un système en production.

Commande MySql :
```
# sudo mysql -u root -p
> CREATE DATABASE moodle; 
> GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,INDEX,ALTER ON moodle.* 
       TO usename@localhost IDENTIFIED BY 'password'; 
> quit 
# sudo mysqladmin -p reload
```

## Serveur Web

### Installer des modules utiles au bon fonctionnement

```
sudo dnf install php-xml php-json
```

### Installer le serveur Apache

```
sudo dnf install httpd
```

- la conf se trouve dans `/etc/httpd/`
  - le fichier de conf principal est `/etc/httpd/conf/httpd.conf`
    - avec `vim` vous pouvez tout virer avec `:g/^ *#.*/d` pour y voir plus clair

**Démarrer le**

```
sudo systemctl enable --now httpd
```

**Ouvrer le bon port du firewall**

```
sudo firewall-cmd --add-port 80/tcp
```

### Installer Git

```
sudo dnf install git -y
```

**Clone du git de moodle**

L'exemple ci-dessous correspond à l'installation d'une version 3.5.X rattachée à la branche « MOODLE_35_STABLE ». Pour tout autre version, veuillez adapter la branche (par exemple 3.6.X => « MOODLE_36_STABLE »).

- Pour utiliser Git, il faut ouvrir les ports 9418 TCP & UDP de votre serveur. Puis :
```
cd /var/www/html/                                                    (1)
sudo git clone git://git.moodle.org/moodle.git                       (2)
cd moodle
sudo git branch --track MOODLE_35_STABLE origin/MOODLE_35_STABLE     (3)
sudo git checkout MOODLE_35_STABLE                                   (4)
```

- (1) On se place dans le répertoire parent (à adapter à votre serveur).
- (2) Installe Moodle via Git : le dossier moodle sera créé et les fichiers y seront téléchargés. L'opération dure plusieurs minutes, car la totalité de l'historique de Moodle est téléchargée.
- (3) Crée une branche locale appelée MOODLE_35_STABLE, qui pourra être synchronisée ultérieurement avec la branche MOODLE_35_STABLE du code source original.
- (4) Utilise la branche locale nouvellement créée.

La suite de l'installation se fait comme pour une installation habituelle :

- Copier le fichier de configuration modèle :
```
cp config-dist.php config.php
```

- On modifie le fichier de config `config.php` :
```
$CFG->dbtype    = 'mysqli'
$CFG->dblibrary = 'native';
$CFG->dbhost    = '10.101.1.12';  // Ip of your db machine
$CFG->dbname    = 'moodle';     // database name, eg moodle
$CFG->dbuser    = 'kai';   // your database username
$CFG->dbpass    = 'manoa';   // your database password

[...]

$CFG->dataroot  = '/home/example/moodledata'
```

- On créer par la suite le moodle data :
```
mkdir /home/example/moodledata
```

### Visiter la page d'administration

La page d'aministration devrait être maintenant active à l'adresse http://ip_serveur_web/moodle/admin. Si vous essayez d'accéder à la page d'accueil de votre site, vous y arriverez directement malgré tout. La première fois que vous accéderez à la page d'administration, un condensé de la licence GPL vous sera présenté. Vous devez l'accepter avant de continuer la mise en service de votre installation.

(Moodle va aussi essayer de placer des cookies dans votre navigateur. Si celui-ci vous permet d'accepter ou non les cookies, vous devez accepter les cookies de Moodle, ou alors Moodle ne fonctionnera pas.)

Moodle va maintenant mettre en place votre base de données et créer les tables qui contiendront les données. Les tables de la base de données principale sont d'abord créées. Un certain nombre de commandes SQL seront affichées, suivies de messages de ce type :

CREATE TABLE course ( id int(10) unsigned NOT NULL auto_increment, category int(10) unsigned NOT NULL default '0', password varchar(50) NOT NULL default '', fullname varchar(254) NOT NULL default '', shortname varchar(15) NOT NULL default '', summary text NOT NULL, format tinyint(4) NOT NULL default '1', teacher varchar(100) NOT NULL default 'Teacher', startdate int(10) unsigned NOT NULL default '0', enddate int(10) unsigned NOT NULL default '0', timemodified int(10) unsigned NOT NULL default '0', PRIMARY KEY (id)) TYPE=MyISAM

SUCCESS

... et ainsi de suite, suivi de : Main databases set up successfully.

Si ces messages n'apparaissent pas, c'est qu'un problème est survenu avec la base de données, ou qu'un paramètre de votre configuration est incorrect dans votre config.php. Vérifiez que PHP ne fonctionne pas en mode « Safe Mode ». Vous pouvez vérifier la configuration des variables PHP en créant un fichier PHP contenant et en le chargeant dans votre navigateur. Vérifiez tout et rechargez ensuite la page d'administration.

Descendez tout en bas de la page et cliquez sur le lien « Continuer ».

Une page semblable va s'afficher, qui met en place toutes les tables requises par chaque module de Moodle. Comme avant, tout doit être en vert.

Descendez tout en bas de la page et cliquez sur le lien « Continuer ».

Un formulaire devrait s'afficher maintenant, dans lequel vous pouvez définir plus d'options de configuration pour votre installation, telles que la langue par défaut, les hôtes SMTP, etc. Si vous n'avez pas encore déterminé tous les réglages, ce n'est pas grave : vous pourrez en tout temps modifier ces réglages à l'aide de l'interface d'administration de Moodle. Descendez tout en bas de la page et cliquez sur le bouton « Enregistrer ».

Si vous êtes coincé sur cette page et ne pouvez continuer (et seulement dans ce cas), c'est probablement que votre serveur souffre de ce que j'appelle le problème du « buggy referrer ». Il est facile d'y remédier : désactivez le réglage « secureforms », puis essayez de continuer.

La page suivante est un formulaire où vous pouvez définir l'aspect de votre site Moodle et de sa page d'accueil, comme le nom, le format, le texte de description, etc. Remplissez-le (modifiable plus tard) et cliquez sur le bouton « Enregistrer ».

Finalement, l'on vous demandera de créer un utilisateur administrateur, qui aura accès aux pages d'administration. Remplissez les différents champs avec votre nom, votre adresse de courriel, etc., puis cliquez sur le bouton « Enregistrer ». Tous les champs ne sont pas obligatoires. Si toutefois vous oubliez de renseigner un champ important, on vous demandera de le remplir.

**Assurez-vous de retenir le nom d'utilisateur et le mot de passe que vous choisissez pour le compte administrateur. Ces données seront nécessaires pour accéder à la page d'administration.**

*(Si pour une raison ou pour une autre l'installation est interrompue, ou si une erreur système vous empêche de vous annoncer dans Moodle avec votre compte administrateur, il est en général possible de s'annoncer avec le nom d'utilisateur par défaut « admin », avec le mot de passe « admin ».)*

# III. Backup

### Setup partage NFS (sur tout les serveurs)

- Installer NFS

```
sudo dnf -y install nfs-utils
```
Création du dossier pour toute les backups
```
sudo mkdir /srv/backup
sudo chown -R kai:kai /srv/backup/ (1)
```
*(1) suivant l'utillisateur*

Modification pour ajouter un Nom de Domaine
```
cat /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp3

[...]
```

Gestion du firewall
```
sudo firewall-cmd --add-service={nfs3,mountd,rpc-bind} --permanent
sudo firewall-cmd --reload
```

## Server Backup

Création des dossiers de backup pour chaque server
```
sudo mkdir -p /srv/backup/{web,db}.tp3
```

Permettre l'écriture sur les dossier de backup suivant le bon server
```
[kai@backup ~]$ cat /etc/exports
/srv/backup/web.tp3 web(rw, no_root_squash) /srv/backup/db.tp3 db(rw, no_root_squash)
```

Démarrer le service NFS
```
sudo systemctl enable --now nfs-server
```

## 1. Server Web

**Setup points de montage sur `web.tp3`**

- Ajouter cette ligne afin que la partition ce monte automatiquement grâce au fichier `/etc/fstab`
```
backup:/srv/backup/web.tp3 /srv/backup/ nfs    defaults        0 0
```

- monter le dossier `/srv/backups/web.tp3` du serveur NFS dans le dossier `/srv/backup/` du serveur Web
```
sudo mount /srv/backup/ -v
```

**Backup du server web**

Télécharger le [script de backup](./Backup.sh) et ajouter un crontab sur votre server web
```
crontab -e
```

Exécution tous les jours à 22h00 d'une backup
```
00 22 * * * /home/example/Backup.sh
```

Démarrer le service crontab
```
sudo systemctl enable --now crond
```

## 2. Server Db

**Setup points de montage sur `db.tp3`**

- Ajouter cette ligne afin que la partition ce monte automatiquement grâce au fichier `/etc/fstab`
```
backup:/srv/backup/db.tp3 /srv/backup/ nfs    defaults        0 0
```

- monter le dossier `/srv/backups/db.tp3` du serveur NFS dans le dossier `/srv/backup/` du serveur db
```
sudo mount /srv/backup/ -v
```

**Backup du server db**

Télécharger le [script de backup](./Backup_db.sh) pour la base de donnée et ajouter un crontab sur votre server db
```
crontab -e
```

Exécution tous les jours à 22h00 d'une backup
```
00 22 * * * /home/example/Backup_db.sh
```

Démarrer le service crontab
```
sudo systemctl enable --now crond
```

# IV. Monitoring (à Faire sur tout les serveurs)

**Télécharger netdata**

```
sudo bash <(curl -Ss https://my-netdata.io/kickstart.sh)
```

Démarrer le service Netdata
```
sudo systemctl enable --now netdata
```

Gérer le firewall
```
sudo firewall-cmd --add-port=19999/tcp --permanent
sudo firewall-cmd --reload
```

Pour accéder à votre monitoring, c'est via votre navigateur à : http://ip_server:19999 

>*Maintenant que votre solution est mise en place, vous pouvez en profiter*
