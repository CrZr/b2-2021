#!/bin/bash
# Simple Script Backup Database
# kai 24/10/21

d=$(date +%y%m%d_%H%M%S);
backup_name=tp2_backup_db_$d.tar.gz;

destination=$(readlink -f $1);
archive=$(readlink -f $backup_name);
database=$(readlink -f database.sql);

mysqldump -u root $2 > database.sql;

tar -czvf $backup_name $database;

rsync -av --remove-source-files  $archive $destination;

# Delete more than 5 backup
cd $destination;
ls -tQ  | tail -n+6 | xargs rm &>/dev/null;
cd -;
rm $database;