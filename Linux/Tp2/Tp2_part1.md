# TP2 pt. 1 : Gestion de service

# I. Un premier serveur web

## 1. Installation

| Machine         | IP            | Service                 | Port ouvert       | IP autorisées |
|-----------------|---------------|-------------------------|-------------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80/tcp     22/tcp | tous          |

🌞 **Installer le serveur Apache**

  - le fichier de conf principal est `/etc/httpd/conf/httpd.conf`
  - je vous conseille **vivement** de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair
  - avec `vim` vous pouvez tout virer avec `:g/^ *#.*/d`

```
ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache


ServerAdmin root@localhost


<Directory />
    AllowOverride none
    Require all denied
</Directory>


DocumentRoot "/var/www/html"

<Directory "/var/www">
    AllowOverride None
    Require all granted
</Directory>

<Directory "/var/www/html">
    Options Indexes FollowSymLinks

    AllowOverride None

    Require all granted
</Directory>

<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

<Files ".ht*">
    Require all denied
</Files>

ErrorLog "logs/error_log"

LogLevel warn

<IfModule log_config_module>
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common

    <IfModule logio_module>
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>


    CustomLog "logs/access_log" combined
</IfModule>

<IfModule alias_module>


    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

</IfModule>

<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>
    TypesConfig /etc/mime.types

    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz



    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

AddDefaultCharset UTF-8

<IfModule mime_magic_module>
    MIMEMagicFile conf/magic
</IfModule>


EnableSendfile on

IncludeOptional conf.d/*.conf
```

🌞 **Démarrer le service Apache**

- le service s'appelle `httpd` (raccourci pour `httpd.service` en réalité)
    - démarrez le
    ```
    [kai@web ~]$ sudo systemctl start httpd.service
    ```
  
  - faites en sorte qu'Apache démarre automatique au démarrage de la machine
  ```
  [kai@web ~]$ sudo systemctl enable httpd.service
  ```
  
  - ouvrez le port firewall nécessaire
  ```
  [kai@web ~]$ sudo firewall-cmd --add-port 80/tcp
  success
  ```
    - utiliser une commande `ss` pour savoir sur quel port tourne actuellement Apache
    ```
    [kai@web ~]$ sudo ss -lanp
    tcp    LISTEN  0   128   *:80    *:*    users:(("httpd",pid=864,fd=4),("httpd",pid=863,fd=4),("httpd",pid=862,fd=4),("httpd",pid=836,fd=4))
    ```

🌞 **TEST**



- vérifier que le service est démarré
```
[kai@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-10-06 09:56:37 CEST; 1h 20min ago
  [...]
```

- vérifier qu'il est configuré pour démarrer automatiquement
```
[kai@web ~]$ sudo systemctl is-enabled httpd
[sudo] password for kai:
enabled
```

- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement
```
[kai@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

[...]

</html>
```

- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web
![](https://i.imgur.com/LQMuC1U.png)


## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume
```
[kai@web ~]$ sudo systemctl enable httpd
```

- prouvez avec une commande qu'actuellement, le service est paramétré pour démarrer quand la machine s'allume
```
[kai@web ~]$ sudo systemctl is-enabled httpd
[sudo] password for kai:
enabled
```

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache
```
[kai@web ~]$ cat /usr/lib/systemd/system/httpd.service

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé
```
User apache
Group apache
```

- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
```
apache       861     836  0 09:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       862     836  0 09:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       863     836  0 09:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       864     836  0 09:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

- vérifiez avec un `ls -al` le dossier du site (dans `/var/www/...`) 
```
[kai@web ~]$ ls -al /var/www/
total 4
drwxr-xr-x.  4 root root   33 Sep 29 12:19 .
drwxr-xr-x. 22 root root 4096 Sep 29 12:19 ..
drwxr-xr-x.  2 root root    6 Jun 11 17:35 cgi-bin
drwxr-xr-x.  2 root root    6 Jun 11 17:35 html
```

  - vérifiez que tout son contenu est accessible à l'utilisateur mentionné dans le fichier de conf
> Le  fichier de conf est accessible en lecture et en exécution par tout le monde donc tout les utilisateurs y ont accés

🌞 **Changer l'utilisateur utilisé par Apache**

- créez le nouvel utilisateur
```
[kai@web ~]$ sudo useradd -r -s /bin/nologin webby
```
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
```
[...]
User webby
Group webby
[...]
```

- redémarrez Apache
```
[kai@web ~]$ sudo systemctl restart httpd
```

- utilisez une commande `ps` pour vérifier que le changement a pris effet
```
[kai@web ~]$ ps -ef | grep httpd
root        1728       1  0 21:01 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webby       1730    1728  0 21:01 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webby       1731    1728  0 21:01 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webby       1732    1728  0 21:01 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webby       1733    1728  0 21:01 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
kai         1946    1657  0 21:02 pts/0    00:00:00 grep --color=auto httpd
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port de votre choix
```
[...]
Listen 12000
[...]
```

- ouvrez un nouveau port firewall, et fermez l'ancien
```
[kai@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[kai@web ~]$ sudo firewall-cmd --add-port=12000/tcp --permanent
success
```

- redémarrez Apache
```
[kai@web ~]$ sudo systemctl restart httpd
```

- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi
```
[kai@web ~]$ sudo ss -lanp
tcp    LISTEN  0    128     *:12000   *:*    users:(("httpd",pid=1515,fd=4),("httpd",pid=1514,fd=4),("httpd",pid=1513,fd=4),("httpd",pid=1511,fd=4))
```
- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port
```
[kai@web ~]$ curl localhost:12000
<!doctype html>
[...]
```

📁 **Fichier [httpd.conf](https://gitlab.com/CrZr/b2-2021/-/tree/main/Linux/Tp2/Fichiers/httpd.conf)**

# II. Une stack web plus avancée

## 1. Intro

```
[kai@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
[...]
IncludeOptional sites-enabled/*
```

## 2. Setup

| Machine         | IP            | Service                 | Port ouvert       | IP autorisées |
|-----------------|---------------|-------------------------|-------------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80/tcp     22/tcp | tous          |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 3306/tcp   22/tcp | 10.102.1.11   |

### A. Serveur Web et NextCloud

🌞 Install du serveur Web et de NextCloud sur `web.tp2.linux`

```
[kai@web ~]$ sudo dnf install -y mariadb-server httpd vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp 
Complete!
```
```
[kai@web ~]$ sudo mkdir /etc/httpd/sites-available
```
```
[kai@web ~]$ sudo nano /etc/httpd/sites-available/linux.tp2.web

<VirtualHost *:80>
  DocumentRoot /var/www/sub-domains/linux.tp2.web/html/
  ServerName  web.tp2.linux

  <Directory /var/www/sub-domains/linux.tp2.web/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```
```
[kai@web ~]$ sudo mkdir /etc/httpd/sites-enabled/
```
```
[kai@web ~]$ sudo ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/
```
```
[kai@web ~]$ sudo mkdir -p /var/www/sub-domains/linux.tp2.web/html/
```
```
[kai@web ~]$ timedatectl
[...]
                Time zone: Europe/Paris (CEST, +0200)
[...]
```
```
[kai@web ~]$ sudo vim /etc/opt/remi/php74/php.ini
[...]
date.timezone = "Europe/Paris"
[...]
```
```
[kai@web ~]$ systemctl enable mariadb
[kai@web ~]$ systemctl restart mariadb
```
```
[kai@web ~]$ wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
[kai@web ~]$ unzip nextcloud-22.2.0.zip
```
```
[kai@web ~]$ cd nextcloud
[kai@web nextcloud]$ sudo mv * /var/www/sub-domains/linux.tp2.web/html/
[kai@web nextcloud]$ sudo chown -Rf apache.apache /var/www/sub-domains/linux.tp2.web/html/
```

📁 **Fichier [httpd_nextcloud.conf](https://gitlab.com/CrZr/b2-2021/-/tree/main/Linux/Tp2/Fichiers/httpd_nextcloud.conf)**  
📁 **Fichier [linux.tp2.web](https://gitlab.com/CrZr/b2-2021/-/tree/main/Linux/Tp2/Fichiers/linux.tp2.web)**

### B. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

- je veux dans le rendu **toutes** les commandes réalisées
```
[kai@db ~]$ sudo dnf install epel-release -y
```
```
[kai@db ~]$ sudo dnf update -y
```
```
[kai@db ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm -y
```
```
[kai@db ~]$ sudo dnf module enable php:remi-7.4 -y
```
```
[kai@db ~]$ sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp -y
```
```
[kai@db ~]$ sudo dnf install -y mariadb-server
```
```
[kai@db ~]$ mysql_secure_installation
```
```
[kai@db ~]$ sudo systemctl enable mariadb
[kai@db ~]$ sudo systemctl restart mariadb
```

- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`
```
[kai@db ~]$ ss -alnpt
LISTEN          0                80                                     *:3306                                *:*
```

MariaDb est donc en écoute sur le port 3306

🌞 **Préparation de la base pour NextCloud**

- une fois en place, il va falloir préparer une base de données pour NextCloud :
  - connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root`
  - exécutez les commandes SQL suivantes :

```sql
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'root';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

🌞 **Exploration de la base de données**

- afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :
  - depuis la machine `web.tp2.linux` vers l'IP de `db.tp2.linux`
  - vous pouvez utiliser la commande `mysql` pour vous connecter à une base de données depuis la ligne de commande
    - par exemple `mysql -u <USER> -h <IP_DATABASE> -p`
```
[kai@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 10
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

- utilisez les commandes SQL fournies ci-dessous pour explorer la base

```sql
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.002 sec)

MariaDB [(none)]> USE nextcloud;
Database changed
MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.001 sec)
```

- trouver une commande qui permet de lister tous les utilisateurs de la base de données
```sql
MariaDB [(none)]> SELECT User, Host FROM mysql.user;
+-----------+-------------+
| User      | Host        |
+-----------+-------------+
| nextcloud | 10.102.1.11 |
| root      | 127.0.0.1   |
| root      | ::1         |
| root      | localhost   |
+-----------+-------------+
```

### C. Finaliser l'installation de NextCloud

🌞 sur votre PC

- modifiez votre fichier `hosts` (oui, celui de votre PC, de votre hôte)
```
PS C:\Users\LDLC> notepad C:\Windows\System32\drivers\etc\hosts
```
> ajout de la ligne `10.102.11 web.tp2.linux`

🌞 **Exploration de la base de données**

- connectez vous en ligne de commande à la base de données après l'installation terminée
```
[kai@db ~]$ sudo mysql -u root -p
[sudo] password for kai:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 148
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```

- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
```
MariaDB [(none)]> use nextcloud;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [nextcloud]> SELECT FOUND_ROWS();
+--------------+
| FOUND_ROWS() |
+--------------+
|          105 |
+--------------+
1 row in set (0.000 sec)
```

> Il y a 105 tables crées par NextCloud lors de la finalisation de l'installation