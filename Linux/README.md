# **TP Linux**  
  - [Tp1 - (re)Familiaration avec un système GNU/Linux](https://gitlab.com/CrZr/b2-2021/-/tree/main/Linux/Tp1)

---

  - [Tp2 pt. 1 - Gestion de service](https://gitlab.com/CrZr/b2-2021/-/tree/main/Linux/Tp2/Tp2_part1.md)
  - [Tp2 pt. 2 - Maintien en condition opérationnelle](https://gitlab.com/CrZr/b2-2021/-/tree/main/Linux/Tp2/Tp2_part2.md)

---

  - [Tp3 - Simple Moodle Server](https://gitlab.com/CrZr/b2-2021/-/tree/main/Linux/Tp3/README.md)
